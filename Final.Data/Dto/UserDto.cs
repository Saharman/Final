﻿using Final.Data.Entities;
using Final.Data.Enum;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Final.Data.Dto
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public IFormFile LoadImg { get; set; }
        public string Img { get; set; } = "/file/defaultUser.jpg";
        public string Name { get; set; }
        public string Surname { get; set; }
        public UserType Type { get; set; }


        public List<Taska> ResponsibleTask { get; set; } = new List<Taska>();//для организатора
        public List<TaskVolunteer> TaskVolunteer { get; set; } = new List<TaskVolunteer>();//для волонтера
        public List<PlanTask> PlanTask { get; set; } = new List<PlanTask>(); // для организатора
    }
}
