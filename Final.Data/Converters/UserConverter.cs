﻿using Final.Data.Dto;
using Final.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Final.Data.Converters
{
    public static class UserConverter
    {
        public static UserDto Convert(User item)
        {
            return new UserDto
            {
                Id = item.Id,
                Email = item.Email,
                Img = item.Img,
                LoadImg = item.LoadImg,
                Name = item.Name,
                Surname = item.Surname,
                PlanTask = item.PlanTask,
                ResponsibleTask = item.ResponsibleTask,
                TaskVolunteer = item.TaskVolunteer,
                Type = item.Type
            };
        }

        public static User Convert(UserDto item)
        {
            return new User
            {
                Id = item.Id,
                Email = item.Email,
                UserName = item.Email,
                Img = item.Img,
                LoadImg = item.LoadImg,
                Name = item.Name,
                Surname = item.Surname,
                Type = item.Type
            };
        }

        public static List<UserDto> Convert(List<User> items)
        {
            return items.Select(a => Convert(a)).ToList();
        }

        public static List<User> Convert(List<UserDto> items)
        {
            return items.Select(a => Convert(a)).ToList();
        }


    }
}
