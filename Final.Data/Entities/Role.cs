﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Final.Data.Entities
{
    public class Role : IdentityRole<Guid>
    {
        public Role(string name) : base(name) { }
    }
}
