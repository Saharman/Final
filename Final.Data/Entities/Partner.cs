﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Final.Data.Entities
{
    public class Partner
    {
        public Guid Id { get; set; }
        public PartnerType Type { get; set; }
        public string Description { get; set; }
        public string Img { get; set; } = "/files/defaultPartner.jpg";
        [NotMapped]
        public IFormFile LoadImg { get; set; }


        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }


        public string INN { get; set; }
        public string Web { get; set; }
        public string Title { get; set; }


        public List<PartnerHistory> Histories { get; set; } = new List<PartnerHistory>();
        public Contact Contact { get; set; }
        public Guid ContactId { get; set; }
        public List<HashTag> HashTags { get; set; } = new List<HashTag>();
        public List<Taska> Tasks { get; set; } = new List<Taska>();
        public List<PartnerEvent> PartnerEvent { get; set; } = new List<PartnerEvent>();
        public List<PlanTask> PlanTask { get; set; } = new List<PlanTask>();
    }
}
