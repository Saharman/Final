﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Final.Data.Entities
{
    public class Speaker
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string Description { get; set; }
        [NotMapped]
        public IFormFile LoadImg {get; set;}
        [Required]
        public string Img { get; set; } = "/files/defaultSpeaker.jpg";
        [Required]
        public Guid EventId { get; set; }
    }
}
