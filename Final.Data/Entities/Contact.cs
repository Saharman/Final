﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Final.Data.Entities
{
    public class Contact
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public string CommunicationMethod { get; set; }
        [NotMapped]
        public IFormFile LoadImg { get; set; }
        [Required]
        public string Img { get; set; } = "/files/defaultContact.jpg";
        [Required]
        public Guid PartnerId { get; set; }
    }
}
