﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Final.Data.Entities
{
    public class Taska
    {
        public Guid Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public Guid ResponsibleId { get; set; }
        [Required]
        public Guid PartnerId { get; set; }
        [Required]
        public Guid EventId { get; set; }
        [Required]
        public TaskStatus Status { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime DeadLine { get; set; }

        public List<TaskVolunteer> TaskVolunteer = new List<TaskVolunteer>();
    }
}
