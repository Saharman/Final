﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Final.Data.Entities
{
    public class HashTag
    {
        public Guid Id { get; set; }
        [Required]
        public string Text { get; set; }
        [Required]
        public Guid PartnerId { get; set; }
    }
}
