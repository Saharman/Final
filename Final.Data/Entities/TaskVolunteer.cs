﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Final.Data.Entities
{
    public class TaskVolunteer
    {
        public Guid TaskId { get; set; }
        public Guid VolunteerId { get; set; }
    }
}
