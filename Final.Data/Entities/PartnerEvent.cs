﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Final.Data.Entities
{
    public class PartnerEvent
    {
        public Guid PartnerId { get; set; }
        public Guid EventId { get; set; }
    }
}
