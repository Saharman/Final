﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Final.Data.Entities
{
    public class Category
    {
        public Guid Id { get; set; }
        [Required]
        public string Title { get; set; }

        public List<Event> Events { get; set; } = new List<Event>();
    }
}
