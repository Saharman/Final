﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Final.Data.Entities
{
    public class PartnerHistory
    {
        public Guid Id { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public string Сhannel { get; set; }
        [Required]
        public string Result { get; set; }
        [Required]
        public string Topic { get; set; }
        [Required]
        public Guid PartnerId { get; set; }
    }
}
