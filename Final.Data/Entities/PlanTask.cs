﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Final.Data.Entities
{
    public class PlanTask
    {
        public Guid Id { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public Guid EventId { get; set; }
        public Event Event { get; set; }
        public List<PlanTaskPartner> PlanTaskPartner { get; set; } = new List<PlanTaskPartner>();
        public Guid ResponsibleId { get; set; }

    }
}
