﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Final.Data.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System;
using Final.Data.Enum;

namespace Final.Data.Entities
{
    public class User: IdentityUser<Guid>
    {
        [NotMapped]
        public IFormFile LoadImg { get; set; }
        public string Img { get; set; } = "/file/defaultUser.jpg";
        public string Name { get; set; }
        public string Surname { get; set; }
        public UserType Type { get; set; }


        public List<Taska> ResponsibleTask { get; set; } = new List<Taska>();//для организатора
        public List<TaskVolunteer> TaskVolunteer { get; set; } = new List<TaskVolunteer>();//для волонтера
        public List<PlanTask> PlanTask { get; set; } = new List<PlanTask>(); // для организатора
    }
}
