﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Final.Data.Entities
{
    public class Event
    {
        public Guid Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public DateTime Start { get; set; }
        [Required]
        public DateTime End { get; set; }
        [Required]
        public string Place { get; set; }
        [NotMapped]
        public IFormFile LoadImg { get; set; }
        [Required]
        public string Img { get; set; } = "/files/defaultEvent.jpg";
        [Required]
        public string Description { get; set; }



        public Guid SpeakerId { get; set; }
        public Speaker Speaker { get; set; }
        public List<PartnerEvent> PartnerEvent { get; set; } = new List<PartnerEvent>();
        public List<Taska> Tasks { get; set; } = new List<Taska>();
        public List<PlanTask> PlanTask { get; set; } = new List<PlanTask>();
    }
}
