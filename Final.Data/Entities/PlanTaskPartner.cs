﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Final.Data.Entities
{
    public class PlanTaskPartner
    {
        public Guid PlanTaskId { get; set; }
        public Guid PartnerId { get; set; }
    }
}
