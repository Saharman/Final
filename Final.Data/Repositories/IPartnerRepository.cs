﻿using Final.Data.Dto;
using Final.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Final.Data.Repositories
{
    public interface IPartnerRepository
    {
        Task<List<Partner>> GetAllAsync();
        Task<Partner> GetByIdAsync(Guid id);
        Task<Partner> CreateAsync(Partner item);
        Task<Partner> UpdateAsync(Partner item);
        Task<bool> DeleteAsync(Guid id);
    }
}
