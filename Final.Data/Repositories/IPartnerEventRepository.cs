﻿using Final.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Final.Data.Repositories
{
    public interface IPartnerEventRepository
    {
        Task<List<PartnerEvent>> GetAllAsync();
        Task<PartnerEvent> GetByIdAsync(Guid partnerId, Guid eventId);
        Task<List<PartnerEvent>> GetByPartnerAsync(Guid id);
        Task<List<PartnerEvent>> GetByEventAsync(Guid id);
        Task<PartnerEvent> CreateAsync(PartnerEvent item);
        Task<bool> DeleteAsync(Guid partnerId, Guid eventId);
    }
}
