﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Final.Data.Entities;


namespace Final.Data.Repositories
{
    public interface ITaskRepository: IRepository<Taska>
    {
        Task<List<Taska>> GetByPartnerAsync(Guid id);
        Task<List<Taska>> GetByEventAsync(Guid id);
        Task<List<Taska>> GetByResponsibleAsync(Guid id);
    }
}
