﻿using Final.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Final.Data.Repositories
{
    public interface IEventRepository: IRepository<Event>
    {
    }
}
