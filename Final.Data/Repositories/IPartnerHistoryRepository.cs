﻿using Final.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Final.Data.Repositories
{
    public interface IPartnerHistoryRepository: IRepository<PartnerHistory>
    {
        Task<List<PartnerHistory>> GetByPartnerAsync(Guid id);
    }
}
