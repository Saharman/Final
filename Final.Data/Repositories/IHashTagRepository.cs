﻿using Final.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Final.Data.Repositories
{
    public interface IHashTagRepository: IRepository<HashTag>
    {
        Task<List<HashTag>> GetByPartnerAsync(Guid id);
    }
}
