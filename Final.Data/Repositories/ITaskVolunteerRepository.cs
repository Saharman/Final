﻿using Final.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Final.Data.Repositories
{
    public interface ITaskVolunteerRepository
    {
        Task<List<TaskVolunteer>> GetAllAsync();
        Task<TaskVolunteer> GetByIdAsync(Guid taskId, Guid userId);
        Task<List<TaskVolunteer>> GetByTaskAsync(Guid id);
        Task<List<TaskVolunteer>> GetByVolunteerAsync(Guid id);
        Task<TaskVolunteer> CreateAsync(TaskVolunteer item);
        Task<bool> DeleteAsync(Guid taskId, Guid userId);
    }
}
