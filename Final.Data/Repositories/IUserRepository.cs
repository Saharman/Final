﻿using Final.Data.Dto;
using Final.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Final.Data.Repositories
{
    public interface IUserRepository
    {
        Task<List<UserDto>> GetAllAsync();
        Task<UserDto> GetByIdAsync(Guid id);
        Task<UserDto> CreateAsync(UserDto item, string password);
        Task<bool> UpdateAsync(UserDto item);
        Task<bool> DeleteAsync(Guid id);
    }
}
