﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Final.Data.Entities
{
    public enum TaskStatus
    {
        Success,
        Fail,
        InProgress
    }
}
