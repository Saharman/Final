﻿using Final.Core.EF;
using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Final.Core.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly FinalContext _context;

        public CategoryRepository(FinalContext context)
        {
            _context = context;
        }

        public async Task<Category> CreateAsync(Category item)
        {
            var result = await _context.Categories.AddAsync(item);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            Category category = await _context.Categories.FindAsync(id);
            if (category == null)
                return false;
            _context.Categories.Remove(category);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<List<Category>> GetAllAsync()
        {
            return await _context.Categories.ToListAsync();
        }

        public async Task<Category> GetByIdAsync(Guid id)
        {
            return await _context.Categories.Include(x => x.Events).FirstAsync(y => y.Id == id);
        }

        public async Task<Category> UpdateAsync(Category item)
        {
            if (item == null || item.Id == null)
                return null;
            _context.Categories.Update(item);
            await _context.SaveChangesAsync();
            return item;
        }
    }
}
