﻿using Final.Core.EF;
using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final.Core.Repositories
{
    public class PartnerHistoryRepository : IPartnerHistoryRepository
    {
        private readonly FinalContext _context;

        public PartnerHistoryRepository(FinalContext context)
        {
            _context = context;
        }

        public async Task<PartnerHistory> CreateAsync(PartnerHistory item)
        {
            var result = await _context.PartnerHistories.AddAsync(item);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            PartnerHistory PartnerHistory = await _context.PartnerHistories.FindAsync(id);
            if (PartnerHistory == null)
                return false;
            _context.PartnerHistories.Remove(PartnerHistory);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<List<PartnerHistory>> GetAllAsync()
        {
            return await _context.PartnerHistories.ToListAsync();
        }

        public async Task<PartnerHistory> GetByIdAsync(Guid id)
        {
            return await _context.PartnerHistories.FindAsync(id);
        }

        public async Task<PartnerHistory> UpdateAsync(PartnerHistory item)
        {
            _context.PartnerHistories.Update(item);
            await _context.SaveChangesAsync();
            return item;
        }


        public async Task<List<PartnerHistory>> GetByPartnerAsync(Guid id)
        {
            return await _context.PartnerHistories.Where(x => x.PartnerId == id).ToListAsync();
        }

    }
}
