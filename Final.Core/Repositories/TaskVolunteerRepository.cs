﻿using Final.Core.EF;
using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Final.Core.Repositories
{
    public class TaskVolunteerRepository : ITaskVolunteerRepository
    {
        private readonly FinalContext _context;

        public TaskVolunteerRepository(FinalContext context)
        {
            _context = context;
        }

        public async Task<TaskVolunteer> CreateAsync(TaskVolunteer item)
        {
            var result = await _context.TaskVolunteers.AddAsync(item);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<bool> DeleteAsync(Guid taskId, Guid userId)
        {
            TaskVolunteer tv = await _context.TaskVolunteers.FirstAsync(x => x.TaskId == taskId && x.VolunteerId == userId);
            if (tv == null)
                return false;
            _context.TaskVolunteers.Remove(tv);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<List<TaskVolunteer>> GetAllAsync()
        {
            return await _context.TaskVolunteers.ToListAsync();
        }

        public async Task<List<TaskVolunteer>> GetByVolunteerAsync(Guid id)
        {
            return await _context.TaskVolunteers.Where(x => x.VolunteerId == id).ToListAsync();
        }

        public async Task<TaskVolunteer> GetByIdAsync(Guid taskId, Guid userId)
        {
            return await _context.TaskVolunteers.FirstAsync(x => x.VolunteerId == userId && x.TaskId == taskId);
        }

        public async Task<List<TaskVolunteer>> GetByTaskAsync(Guid id)
        {
            return await _context.TaskVolunteers.Where(x => x.TaskId == id).ToListAsync();
        }
    }
}
