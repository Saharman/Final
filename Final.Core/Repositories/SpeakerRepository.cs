﻿using Final.Core.EF;
using Final.Core.Services;
using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Final.Core.Repositories
{
    public class SpeakerRepository : ISpeakerRepository
    {
        private readonly FinalContext _context;
        private readonly IFileLoader _loader;

        public SpeakerRepository(FinalContext context, IFileLoader loader)
        {
            _context = context;
            _loader = loader;
        }

        public async Task<Speaker> CreateAsync(Speaker item)
        {
            if (item.LoadImg != null)
                item.Img = await _loader.LoadImg(item.LoadImg);
            var result = await _context.Speakers.AddAsync(item);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            Speaker Speaker = await _context.Speakers.FindAsync(id);
            if (Speaker == null)
                return false;
            _context.Speakers.Remove(Speaker);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<List<Speaker>> GetAllAsync()
        {
            return await _context.Speakers.ToListAsync();
        }

        public async Task<Speaker> GetByIdAsync(Guid id)
        {
            return await _context.Speakers.FindAsync(id);
        }

        public async Task<Speaker> UpdateAsync(Speaker item)
        {
            if (item.LoadImg != null)
                item.Img = await _loader.LoadImg(item.LoadImg);
            _context.Speakers.Update(item);
            await _context.SaveChangesAsync();
            return item;
        }

        public Task<Speaker> GetByEventAsync(Guid id)
        {
            return _context.Speakers.FirstAsync(x => x.EventId == id);
        }
    }
}
