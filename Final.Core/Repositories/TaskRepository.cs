﻿using Final.Core.EF;
using Final.Core.Services;
using Final.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Final.Data.Entities;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Final.Core.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        private readonly FinalContext _context;

        public TaskRepository(FinalContext context)
        {
            _context = context;
        }

        public async Task<Taska> CreateAsync(Taska item)
        {
            var result = await _context.Tasks.AddAsync(item);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            Taska Task = await _context.Tasks.FindAsync(id);
            if (Task == null)
                return false;
            _context.Tasks.Remove(Task);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<List<Taska>> GetAllAsync()
        {
            return await _context.Tasks.ToListAsync();
        }

        public async Task<Taska> GetByIdAsync(Guid id)
        {
            return await _context.Tasks.Include(x => x.TaskVolunteer).FirstAsync(a => a.Id == id);
        }

        public async Task<Taska> UpdateAsync(Taska item)
        {
            _context.Tasks.Update(item);
            await _context.SaveChangesAsync();
            return item;
        }

        public async Task<List<Taska>> GetByResponsibleAsync(Guid id)
        {
            return await _context.Tasks.Where(x => x.ResponsibleId == id).ToListAsync();
        }

        public async Task<List<Taska>> GetByPartnerAsync(Guid id)
        {
            return await _context.Tasks.Where(x => x.PartnerId == id).ToListAsync();
        }

        public async Task<List<Taska>> GetByEventAsync(Guid id)
        {
            return await _context.Tasks.Where(x => x.EventId == id).ToListAsync();
        }
    }
}
