﻿using Final.Core.EF;
using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final.Core.Repositories
{
    public class PartnerEventRepository: IPartnerEventRepository
    {
        private readonly FinalContext _context;

        public PartnerEventRepository(FinalContext context)
        {
            _context = context;
        }

        public async Task<PartnerEvent> CreateAsync(PartnerEvent item)
        { 
            var result = await _context.PartnerEvent.AddAsync(item);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<bool> DeleteAsync(Guid partnerId, Guid eventId)
        {
            PartnerEvent pe = await _context.PartnerEvent.FirstAsync(x => x.PartnerId == partnerId && x.EventId == eventId);
            if (pe == null)
                return false;
            _context.PartnerEvent.Remove(pe);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<List<PartnerEvent>> GetAllAsync()
        {
            return await _context.PartnerEvent.ToListAsync();
        }

        public async Task<List<PartnerEvent>> GetByEventAsync(Guid id)
        {
            return await _context.PartnerEvent.Where(x => x.EventId == id).ToListAsync();
        }

        public async Task<PartnerEvent> GetByIdAsync(Guid partnerId, Guid eventId)
        {
            return await _context.PartnerEvent.FirstAsync(x => x.EventId == eventId && x.PartnerId == partnerId);
        }

        public async Task<List<PartnerEvent>> GetByPartnerAsync(Guid id)
        {
            return await _context.PartnerEvent.Where(x => x.PartnerId == id).ToListAsync();
        }
    }
}
