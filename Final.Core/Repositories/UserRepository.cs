﻿using Final.Core.EF;
using Final.Core.Services;
using Final.Data.Converters;
using Final.Data.Dto;
using Final.Data.Entities;
using Final.Data.Enum;
using Final.Data.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final.Core.Repositories
{
    public class UserRepository: IUserRepository
    {
        private readonly UserManager<User> _userManager;
        private readonly FinalContext _context;
        private readonly IFileLoader _loader;

        public UserRepository(UserManager<User> userManager, IFileLoader loader, FinalContext context)
        {
            _userManager = userManager;
            _loader = loader;
            _context = context;
        }

        public async Task<UserDto> CreateAsync(UserDto item, string password)
        {
            if (password == null)
                return null;
            if (item.LoadImg != null)
                item.Img = await _loader.LoadImg(item.LoadImg);
            User user = UserConverter.Convert(item);
            var result = await _userManager.CreateAsync(user, password);
            if (!result.Succeeded)
                return null;
            return UserConverter.Convert(await _userManager.FindByEmailAsync(item.Email));
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            User user = await _userManager.FindByIdAsync(id.ToString());
            if (user == null)
                return false;
            var result = await _userManager.DeleteAsync(user);
            return result.Succeeded;
        }

        public async Task<List<UserDto>> GetAllAsync()
        {
            return UserConverter.Convert(await _userManager.Users.ToListAsync());
        }

        public async Task<UserDto> GetByIdAsync(Guid id)
        {
            User user = await _userManager.FindByIdAsync(id.ToString());
            if (user == null)
                return null;
            if (user.Type == UserType.user)
                user.TaskVolunteer = await _context.TaskVolunteers.Where(x => x.VolunteerId == id).ToListAsync();
            else
            {
                user.ResponsibleTask = await _context.Tasks.Where(x => x.ResponsibleId == id).ToListAsync();
                user.PlanTask = await _context.PlanTasks.Where(x => x.ResponsibleId == id).ToListAsync();
            }
            return UserConverter.Convert(user);
        }

        public async Task<bool> UpdateAsync(UserDto item)
        {
            if (item.LoadImg != null)
                item.Img = await _loader.LoadImg(item.LoadImg);
            User user = await _userManager.FindByIdAsync(item.Id.ToString());
            if (user == null)
                return false;
            var result = await _userManager.UpdateAsync(user);
            return result.Succeeded;
        }

    }

}

