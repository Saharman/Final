﻿using Final.Core.EF;
using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final.Core.Repositories
{
    public class HashTagRepository : IHashTagRepository
    {
        private readonly FinalContext _context;

        public HashTagRepository(FinalContext context)
        {
            _context = context;
        }

        public async Task<HashTag> CreateAsync(HashTag item)
        {
            var result = await _context.HashTags.AddAsync(item);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            HashTag HashTag = await _context.HashTags.FindAsync(id);
            if (HashTag == null)
                return false;
            _context.HashTags.Remove(HashTag);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<List<HashTag>> GetAllAsync()
        {
            return await _context.HashTags.ToListAsync();
        }

        public async Task<HashTag> GetByIdAsync(Guid id)
        {
            return await _context.HashTags.FindAsync(id);
        }

        public async Task<HashTag> UpdateAsync(HashTag item)
        {
            if (item == null || item.Id == null)
                return null;
            _context.HashTags.Update(item);
            await _context.SaveChangesAsync();
            return item;
        }

        public async Task<List<HashTag>> GetByPartnerAsync(Guid id)
        {
            return await _context.HashTags.Where(x => x.Id == id).ToListAsync();
        }
    }
}
