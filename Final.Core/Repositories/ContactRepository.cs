﻿using Final.Core.EF;
using Final.Core.Services;
using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final.Core.Repositories
{
    public class ContactRepository : IContactRepository
    {
        private readonly FinalContext _context;
        private readonly IFileLoader _loader;

        public ContactRepository(FinalContext context, IFileLoader loader)
        {
            _context = context;
            _loader = loader;
        }

        public async Task<Contact> CreateAsync(Contact item)
        {
            if (item.LoadImg != null)
                item.Img = await _loader.LoadImg(item.LoadImg);
            var result = await _context.Contacts.AddAsync(item);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            Contact contact = await _context.Contacts.FindAsync(id);
            if (contact == null)
                return false;
            _context.Contacts.Remove(contact);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<List<Contact>> GetAllAsync()
        {
            return await _context.Contacts.ToListAsync();
        }

        public async Task<Contact> GetByIdAsync(Guid id)
        {
            return await _context.Contacts.FindAsync(id);
        }

        public async Task<Contact> UpdateAsync(Contact item)
        {
            if (item.LoadImg != null)
                item.Img = await _loader.LoadImg(item.LoadImg);
            _context.Contacts.Update(item);
            await _context.SaveChangesAsync();
            return item;
        }

        public async Task<Contact> GetByPartner(Guid id)
        {
            return await _context.Contacts.FirstAsync(x => x.PartnerId == id);
        }
    }
}
