﻿using Final.Core.EF;
using Final.Core.Services;
using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Final.Core.Repositories
{
    public class EventRepository : IEventRepository
    {
        private readonly FinalContext _context;
        private readonly IFileLoader _loader;

        public EventRepository(FinalContext context, IFileLoader loader)
        {
            _context = context;
            _loader = loader;
        }

        public async Task<Event> CreateAsync(Event item)
        {
            if (item.LoadImg != null)
                item.Img = await _loader.LoadImg(item.LoadImg);
            var result = await _context.Events.AddAsync(item);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            Event Event = await _context.Events.FindAsync(id);
            if (Event == null)
                return false;
            _context.Events.Remove(Event);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<List<Event>> GetAllAsync()
        {
            return await _context.Events.ToListAsync();
        }

        public async Task<Event> GetByIdAsync(Guid id)
        {
            return await _context.Events
                .Include(x => x.PartnerEvent)
                .Include(y => y.Speaker)
                .Include(a => a.Tasks)
                .Include(c => c.PlanTask)
                .FirstAsync(z => z.Id == id);
        }

        public async Task<Event> UpdateAsync(Event item)
        {
            if (item == null || item.Id == null)
                return null;
            if (item.LoadImg != null)
                item.Img = await _loader.LoadImg(item.LoadImg);
            _context.Events.Update(item);
            await _context.SaveChangesAsync();
            return item;
        }
    }
}
