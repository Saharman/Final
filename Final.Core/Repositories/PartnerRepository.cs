﻿using Final.Core.EF;
using Final.Core.Services;
using Final.Data.Converters;
using Final.Data.Dto;
using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Final.Core.Repositories
{
    public class PartnerRepository: IPartnerRepository
    {
        private readonly FinalContext _context;
        private readonly IFileLoader _loader;

        public PartnerRepository(FinalContext context, IFileLoader loader)
        {
            _context = context;
            _loader = loader;
        }

        public async Task<Partner> CreateAsync(Partner item)
        {
            if (item.LoadImg != null)
                item.Img = await _loader.LoadImg(item.LoadImg);
            var result = await _context.Partners.AddAsync(item);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var partner = await _context.Partners.FindAsync(id);
            if (partner == null)
                return false;
            _context.Partners.Remove(partner);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<List<Partner>> GetAllAsync()
        {
            return await _context.Partners.ToListAsync();
        }

        public async Task<Partner> GetByIdAsync(Guid id)
        {
            return await _context.Partners
                .Include(x => x.Contact)
                .Include(v => v.Histories)
                .Include(y => y.HashTags)
                .Include(b => b.Tasks)
                .Include(d => d.PartnerEvent)
                .Include(e => e.PlanTask)
                .FirstAsync(a => a.Id == id);
        }

        public async Task<Partner> UpdateAsync(Partner item)
        {
         
            if (item.LoadImg != null)
                item.Img = await _loader.LoadImg(item.LoadImg);
            await _context.Partners.AddAsync(item);
            await _context.SaveChangesAsync();
            return item;
        }
    }
}
