﻿using Final.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Final.Core.EF
{
    public class FinalContext: IdentityDbContext<User, Role, Guid>
    {
        public FinalContext(DbContextOptions<FinalContext> opt): base(opt)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<PartnerEvent>()
                .HasKey(pe => new { pe.PartnerId, pe.EventId });

            modelBuilder.Entity<TaskVolunteer>()
                .HasKey(tv => new { tv.VolunteerId, tv.TaskId });

            modelBuilder.Entity<PlanTaskPartner>()
                .HasKey(pp => new { pp.PartnerId, pp.PlanTaskId });
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<HashTag> HashTags { get; set; }
        public DbSet<Partner> Partners { get; set; }
        public DbSet<PartnerEvent> PartnerEvent { get; set; }
        public DbSet<PartnerHistory> PartnerHistories { get; set; }
        public DbSet<PlanTask> PlanTasks { get; set; }
        public DbSet<PlanTaskPartner> PlanTaskPartner { get; set; }
        public DbSet<Speaker> Speakers { get; set; }
        public DbSet<Taska> Tasks { get; set; }
        public DbSet<TaskVolunteer> TaskVolunteers { get; set; }
    }
}
