﻿using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Controllers
{
    [Route("api/[controller]")]
    public class SpeakerController: Controller
    {
        private readonly ISpeakerRepository _repo;

        public SpeakerController(ISpeakerRepository repo) => _repo = repo;

        [HttpGet]
        [Produces(typeof(List<Speaker>))]
        public async Task<ActionResult<List<Speaker>>> Get()
        {
            try
            {
                return Ok(await _repo.GetAllAsync());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("{id}")]
        [Produces(typeof(Speaker))]
        public async Task<ActionResult<Speaker>> Get(Guid id)
        {
            try
            {
                return Ok(await _repo.GetByIdAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("event/{id}")]
        [Produces(typeof(Speaker))]
        public async Task<ActionResult<Speaker>> GetByEvent(Guid id)
        {
            try
            {
                return Ok(await _repo.GetByEventAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
      //  [Authorize(Roles = "admin,organiser")]
        [Produces(typeof(Speaker))]
        public async Task<ActionResult<Speaker>> Post([FromBody] Speaker item)
        {
            try
            {
                return Ok(await _repo.CreateAsync(item));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPut]
        [Authorize(Roles = "admin,organiser")]
        [Produces(typeof(Speaker))]
        public async Task<ActionResult<Speaker>> Put([FromBody] Speaker item)
        {
            try
            {
                Speaker cat = await _repo.UpdateAsync(item);
                if (cat == null)
                    return BadRequest();
                return Ok(cat);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin,organiser")]
        [Produces(typeof(bool))]
        public async Task<ActionResult<bool>> Delete(Guid id)
        {
            try
            {
                if (await _repo.DeleteAsync(id))
                    return true;
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}
