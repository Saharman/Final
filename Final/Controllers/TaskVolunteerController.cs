﻿using Final.Core.Repositories;
using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Controllers
{
    [Route("api/[controller]")]
    public class TaskVolunteerController:Controller
    {
        private readonly ITaskVolunteerRepository _repo;

        public TaskVolunteerController(ITaskVolunteerRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        [Produces(typeof(List<TaskVolunteer>))]
        public async Task<ActionResult<List<TaskVolunteer>>> Get()
        {
            try
            {
                return Ok(await _repo.GetAllAsync());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("{taskId}/{userId}")]
        [Produces(typeof(TaskVolunteer))]
        public async Task<ActionResult<TaskVolunteer>> Get(Guid taskId, Guid userId)
        {
            try
            {
                return Ok(await _repo.GetByIdAsync(taskId, userId));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("task/{id}")]
        [Produces(typeof(TaskVolunteer))]
        public async Task<ActionResult<TaskVolunteer>> GetByTask(Guid id)
        {
            try
            {
                return Ok(await _repo.GetByTaskAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("volunteer/{id}")]
        [Produces(typeof(TaskVolunteer))]
        public async Task<ActionResult<TaskVolunteer>> GetByVolunteer(Guid id)
        {
            try
            {
                return Ok(await _repo.GetByVolunteerAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


        [HttpPost]
        [Authorize(Roles = "admin,organiser")]
        [Produces(typeof(TaskVolunteer))]
        public async Task<ActionResult<TaskVolunteer>> Post([FromBody] TaskVolunteer item)
        {
            try
            {
                return Ok(await _repo.CreateAsync(item));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


        [HttpDelete("{taskId}/{userId}")]
        [Authorize(Roles = "admin,organiser")]
        [Produces(typeof(bool))]
        public async Task<ActionResult<bool>> Delete(Guid taskId, Guid userId)
        {
            try
            {
                return Ok(await _repo.DeleteAsync(taskId, userId));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}
