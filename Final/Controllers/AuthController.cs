﻿using Final.Data.Entities;
using Final.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using NETCore.MailKit.Core;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Final.Controllers
{
    [Route("api/auth/[action]")]
    public class AuthController: Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly IConfiguration _configuration;
        private readonly SignInManager<User> _signInManager;
        private readonly IEmailService _EmailService;

        public AuthController(UserManager<User> userManager, RoleManager<Role> roleManager,
            IConfiguration configuration, SignInManager<User> signInManager, IEmailService emailService)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
            _signInManager = signInManager;
            _EmailService = emailService;
        }

        [HttpGet("{email}/{password}")]
        [AllowAnonymous]
        public async Task<object> FastReg(LoginForm form)
        {
            try
            {
                var user = new User
                {
                    UserName = form.Email,
                    Email = form.Email
                };

                var result = await _userManager.CreateAsync(user, form.Password);

                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, false);
                    await _userManager.AddToRoleAsync(user, "user");
                    _EmailService.Send(form.Email, "FastReg", $"Email:{form.Email} \n Password:{form.Password}");
                    return await GenerateJwtToken(user);
                }

                return BadRequest();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }

        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<object> Login([FromBody] LoginForm form)
        {
            try
            {
                var result = await _signInManager.PasswordSignInAsync(form.Email, form.Password, false, false);

                if (result.Succeeded)
                {
                    var appUser = await _userManager.FindByEmailAsync(form.Email);
                    return await GenerateJwtToken(appUser);
                }

                return BadRequest();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }

        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<object> Register([FromBody] LoginForm form)
        {
            try
            {
                var user = new User
                {
                    UserName = form.Email,
                    Email = form.Email
                };

                var result = await _userManager.CreateAsync(user, form.Password);

                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, false);
                    await _userManager.AddToRoleAsync(user, "admin");
                    return await GenerateJwtToken(user);
                }

                return BadRequest();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }

        }



        private async Task<object> GenerateJwtToken(User user)
        {
            string role;

            if (await _userManager.IsInRoleAsync(user, "admin"))
                role = "admin";
            else if (await _userManager.IsInRoleAsync(user, "partner"))
                role = "partner";
            else
                role = "user";

            var claims = new List<Claim>
            {
                new Claim("userEmail", user.Email),
                new Claim("userId", user.Id.ToString()),
                new Claim("role", role)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("Key").ToString()));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(30));

            var token = new JwtSecurityToken(
                _configuration.GetSection("Issuer").ToString(),
                _configuration.GetSection("Issuer").ToString(),
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

    }
}
