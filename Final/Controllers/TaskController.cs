﻿using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Final.Controllers
{
    [Route("api/[controller]")]
    public class TaskController: Controller
    {
        private readonly ITaskRepository _repo;

        public TaskController(ITaskRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        [Produces(typeof(List<Taska>))]
        public async Task<ActionResult<List<Taska>>> Get()
        {
            try
            {
                return Ok(await _repo.GetAllAsync());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("{id}")]
        [Produces(typeof(Taska))]
        public async Task<ActionResult<Taska>> Get(Guid id)
        {
            try
            {
                return Ok(await _repo.GetByIdAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("partner/{id}")]
        [Produces(typeof(Task))]
        public async Task<ActionResult<Taska>> GetByPartner(Guid id)
        {
            try
            {
                return Ok(await _repo.GetByPartnerAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("event/{id}")]
        [Produces(typeof(Taska))]
        public async Task<ActionResult<Taska>> GetByEvent(Guid id)
        {
            try
            {
                return Ok(await _repo.GetByEventAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("responsible/{id}")]
        [Produces(typeof(Task))]
        public async Task<ActionResult<Taska>> GetByResponsible(Guid id)
        {
            try
            {
                return Ok(await _repo.GetByResponsibleAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


        [HttpPost]
        [Authorize(Roles = "admin,organiser")]
        [Produces(typeof(Task))]
        public async Task<ActionResult<Taska>> Post([FromBody] Taska item)
        {
            try
            {
                return Ok(await _repo.CreateAsync(item));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPut]
        [Authorize(Roles = "admin,organiser")]
        [Produces(typeof(Task))]
        public async Task<ActionResult<Taska>> Put([FromBody] Taska item)
        {
            try
            {
                return Ok(await _repo.UpdateAsync(item));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin,organiser")]
        [Produces(typeof(bool))]
        public async Task<ActionResult<bool>> Delete(Guid id)
        {
            try
            {
                return Ok(await _repo.DeleteAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}
