﻿using Final.Core.EF;
using Final.Data.Dto;
using Final.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Controllers
{
    [Route("api/[controller]")]
    public class UserController:Controller
    {
        private readonly IUserRepository _repo;

        public UserController(IUserRepository repo) => _repo = repo;

        [HttpGet]
        [Produces(typeof(List<UserDto>))]
        [Authorize(Roles = "user")]
        public async Task<ActionResult<List<UserDto>>> Get()
        {
            try
            {
                return Ok(await _repo.GetAllAsync());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("{id}")]
        [Produces(typeof(UserDto))]
        public async Task<ActionResult<UserDto>> Get(Guid id)
        {
            try
            {
                return Ok(await _repo.GetByIdAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        [Produces(typeof(UserDto))]
        public async Task<ActionResult<UserDto>> Post([FromBody] UserDto item, string password)
        {
            try
            {
                return Ok(await _repo.CreateAsync(item, password));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPut]
        [Authorize(Roles = "admin")]
        [Produces(typeof(bool))]
        public async Task<ActionResult<bool>> Put([FromBody] UserDto item)
        {
            try
            {
                return Ok(await _repo.UpdateAsync(item));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        [Produces(typeof(bool))]
        public async Task<ActionResult<bool>> Delete(Guid id)
        {
            try
            {
                if (await _repo.DeleteAsync(id))
                    return true;
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}
