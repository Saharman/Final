﻿using Final.Data.Dto;
using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Controllers
{
    [Route("api/[controller]")]
    public class RoleController: Controller
    {
        private readonly RoleManager<Role> _roleManager;
        private readonly UserManager<User> _userManager;
        private readonly IUserRepository _userRepository;

        public RoleController(RoleManager<Role> roleManager, IUserRepository userRepository)
        {
            _roleManager = roleManager;
            _userRepository = userRepository;
        }

        [HttpPost("{name}")]
        [Produces(typeof(bool))]
        public async Task<ActionResult<bool>> Post(string name)
        {
            try
            {
                var a = await _roleManager.CreateAsync(new Role(name));
                //await _roleManager.CreateAsync(new Role("admin"));
                return a.Succeeded;

            }
            catch (Exception ex)
            {

                return StatusCode(500, ex);
            }
        }


    }
}
