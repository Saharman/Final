﻿using Final.Data.Dto;
using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Controllers
{
    [Route("api/[controller]")]
    public class PartnerController : Controller
    {
        private readonly IPartnerRepository _repo;

        public PartnerController(IPartnerRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        [Produces(typeof(List<Partner>))]
        public async Task<ActionResult<List<Partner>>> Get()
        {
            try
            {
                return Ok(await _repo.GetAllAsync());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("{id}")]
        [Produces(typeof(Partner))]
        public async Task<ActionResult<Partner>> Get(Guid id)
        {
            try
            {
                return Ok(await _repo.GetByIdAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
       // [Authorize(Roles = "admin,organiser")]
        [Produces(typeof(Partner))]
        public async Task<ActionResult<Partner>> Post([FromBody] Partner item)
        {
            try
            {
                return Ok(await _repo.CreateAsync(item));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPut]
        [Authorize(Roles = "admin,organiser")]
        [Produces(typeof(Partner))]
        public async Task<ActionResult<Partner>> Put([FromBody] Partner item)
        {
            try
            {
                return Ok(await _repo.UpdateAsync(item));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin,organiser")]
        [Produces(typeof(bool))]
        public async Task<ActionResult<bool>> Delete(Guid id)
        {
            try
            {
                if (await _repo.DeleteAsync(id))
                    return true;
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}
