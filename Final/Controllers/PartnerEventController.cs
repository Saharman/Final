﻿using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Controllers
{
    [Route("api/[controller]")]
    public class PartnerEventController: Controller
    {
        private readonly IPartnerEventRepository _repo;

        public PartnerEventController(IPartnerEventRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        [Produces(typeof(List<PartnerEvent>))]
        public async Task<ActionResult<List<PartnerEvent>>> Get()
        {
            try
            {
                return Ok(await _repo.GetAllAsync());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("{partnerId}/{eventId}")]
        [Produces(typeof(PartnerEvent))]
        public async Task<ActionResult<PartnerEvent>> Get(Guid partnerId, Guid eventId)
        {
            try
            {
                return Ok(await _repo.GetByIdAsync(partnerId, eventId));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("partner/{id}")]
        [Produces(typeof(PartnerEvent))]
        public async Task<ActionResult<PartnerEvent>> GetByPartner(Guid id)
        {
            try
            {
                return Ok(await _repo.GetByPartnerAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("event/{id}")]
        [Produces(typeof(PartnerEvent))]
        public async Task<ActionResult<PartnerEvent>> GetByEvent(Guid id)
        {
            try
            {
                return Ok(await _repo.GetByEventAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


        [HttpPost]
        [Authorize(Roles = "admin,organiser")]
        [Produces(typeof(PartnerEvent))]
        public async Task<ActionResult<PartnerEvent>> Post([FromBody] PartnerEvent item)
        {
            try
            {
                return Ok(await _repo.CreateAsync(item));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


        [HttpDelete("{partnerId}/{eventId}")]
        [Authorize(Roles = "admin,organiser")]
        [Produces(typeof(bool))]
        public async Task<ActionResult<bool>> Delete(Guid partnerId, Guid eventId)
        {
            try
            {
                return Ok(await _repo.DeleteAsync(partnerId, eventId));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}
