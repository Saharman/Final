﻿function eventCard(e) {
    return `<div class="foo col-md-6 col-lg-4">
                  <div class="event_card card">
                    <img class="card-img-top" src="${e.img}" alt="Card image cap">
                    <div class="card-body">
                      <h5 class="card-title">${e.title}</h5>
                      <p class="card-text">${e.description}</p>
                      <a href="#${e.id}" data-id="${e.id}" class="btn btn-primary e_info">Инфо</a>
                    </div>
                  </div>
                </div>`
};