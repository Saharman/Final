﻿function getEvents() {
    $.ajax(
        {
            type: 'GET',
            url: 'http://localhost:5030/api/event',
            dataType: "json",
            success: function (data, textStatus) {
                let event = "";
                data.forEach(function (item) {
                    event = event + eventCard(item);
                });
                $('#events').html(event);
            }
        });
}

function getEvent(id) {
    $.ajax(
        {
            type: 'GET',
            url: `http://localhost:5030/api/event/${id}`,
            dataType: "json",
            success: function (data, textStatus) {
                console.log(data);
                $('#speaker_fio').html(data.speaker.name + ' ' + data.speaker.surname);
                $('#event_title').html(data.title);
                $('#event_date').html(data.start);
                $('#event_place').html(data.place);
                $('#event_description').html(data.description);
            }
        });
}


