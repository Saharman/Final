﻿// <auto-generated />
using System;
using Final.Core.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Final.Migrations
{
    [DbContext(typeof(FinalContext))]
    partial class FinalContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.1-servicing-10028")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Final.Data.Entities.Category", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Title")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Categories");
                });

            modelBuilder.Entity("Final.Data.Entities.Contact", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CommunicationMethod")
                        .IsRequired();

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("Img")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<Guid>("PartnerId");

                    b.Property<string>("Phone")
                        .IsRequired();

                    b.Property<string>("Surname")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Contacts");
                });

            modelBuilder.Entity("Final.Data.Entities.Event", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("CategoryId");

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<DateTime>("End");

                    b.Property<string>("Img")
                        .IsRequired();

                    b.Property<string>("Place")
                        .IsRequired();

                    b.Property<Guid>("SpeakerId");

                    b.Property<Guid?>("SpeakerId1");

                    b.Property<DateTime>("Start");

                    b.Property<string>("Title")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.HasIndex("SpeakerId1");

                    b.ToTable("Events");
                });

            modelBuilder.Entity("Final.Data.Entities.HashTag", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("PartnerId");

                    b.Property<string>("Text")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("PartnerId");

                    b.ToTable("HashTags");
                });

            modelBuilder.Entity("Final.Data.Entities.Partner", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("ContactId");

                    b.Property<Guid?>("ContactId1");

                    b.Property<string>("Description");

                    b.Property<string>("Email");

                    b.Property<string>("INN");

                    b.Property<string>("Img");

                    b.Property<string>("Name");

                    b.Property<string>("Phone");

                    b.Property<string>("Surname");

                    b.Property<string>("Title");

                    b.Property<int>("Type");

                    b.Property<string>("Web");

                    b.HasKey("Id");

                    b.HasIndex("ContactId1");

                    b.ToTable("Partners");
                });

            modelBuilder.Entity("Final.Data.Entities.PartnerEvent", b =>
                {
                    b.Property<Guid>("PartnerId");

                    b.Property<Guid>("EventId");

                    b.HasKey("PartnerId", "EventId");

                    b.HasIndex("EventId");

                    b.ToTable("PartnerEvent");
                });

            modelBuilder.Entity("Final.Data.Entities.PartnerHistory", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date");

                    b.Property<Guid>("PartnerId");

                    b.Property<string>("Result")
                        .IsRequired();

                    b.Property<string>("Topic")
                        .IsRequired();

                    b.Property<string>("Сhannel")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("PartnerId");

                    b.ToTable("PartnerHistories");
                });

            modelBuilder.Entity("Final.Data.Entities.PlanTask", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<Guid>("EventId");

                    b.Property<Guid?>("PartnerId");

                    b.Property<Guid>("ResponsibleId");

                    b.Property<Guid?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("EventId");

                    b.HasIndex("PartnerId");

                    b.HasIndex("UserId");

                    b.ToTable("PlanTasks");
                });

            modelBuilder.Entity("Final.Data.Entities.PlanTaskPartner", b =>
                {
                    b.Property<Guid>("PartnerId");

                    b.Property<Guid>("PlanTaskId");

                    b.HasKey("PartnerId", "PlanTaskId");

                    b.HasIndex("PlanTaskId");

                    b.ToTable("PlanTaskPartner");
                });

            modelBuilder.Entity("Final.Data.Entities.Role", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Final.Data.Entities.Speaker", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<Guid>("EventId");

                    b.Property<string>("Img")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Surname")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Speakers");
                });

            modelBuilder.Entity("Final.Data.Entities.TaskVolunteer", b =>
                {
                    b.Property<Guid>("VolunteerId");

                    b.Property<Guid>("TaskId");

                    b.Property<Guid?>("UserId");

                    b.HasKey("VolunteerId", "TaskId");

                    b.HasIndex("UserId");

                    b.ToTable("TaskVolunteers");
                });

            modelBuilder.Entity("Final.Data.Entities.Taska", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DeadLine");

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<Guid>("EventId");

                    b.Property<Guid>("PartnerId");

                    b.Property<Guid>("ResponsibleId");

                    b.Property<int>("Status");

                    b.Property<string>("Title")
                        .IsRequired();

                    b.Property<Guid?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("EventId");

                    b.HasIndex("PartnerId");

                    b.HasIndex("UserId");

                    b.ToTable("Tasks");
                });

            modelBuilder.Entity("Final.Data.Entities.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("Img");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("Name");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<string>("Surname");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<int>("Type");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<System.Guid>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<Guid>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<System.Guid>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<Guid>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<System.Guid>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<Guid>("UserId");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<System.Guid>", b =>
                {
                    b.Property<Guid>("UserId");

                    b.Property<Guid>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<System.Guid>", b =>
                {
                    b.Property<Guid>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("Final.Data.Entities.Event", b =>
                {
                    b.HasOne("Final.Data.Entities.Category")
                        .WithMany("Events")
                        .HasForeignKey("CategoryId");

                    b.HasOne("Final.Data.Entities.Speaker", "Speaker")
                        .WithMany()
                        .HasForeignKey("SpeakerId1");
                });

            modelBuilder.Entity("Final.Data.Entities.HashTag", b =>
                {
                    b.HasOne("Final.Data.Entities.Partner")
                        .WithMany("HashTags")
                        .HasForeignKey("PartnerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Final.Data.Entities.Partner", b =>
                {
                    b.HasOne("Final.Data.Entities.Contact", "Contact")
                        .WithMany()
                        .HasForeignKey("ContactId1");
                });

            modelBuilder.Entity("Final.Data.Entities.PartnerEvent", b =>
                {
                    b.HasOne("Final.Data.Entities.Event")
                        .WithMany("PartnerEvent")
                        .HasForeignKey("EventId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Final.Data.Entities.Partner")
                        .WithMany("PartnerEvent")
                        .HasForeignKey("PartnerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Final.Data.Entities.PartnerHistory", b =>
                {
                    b.HasOne("Final.Data.Entities.Partner")
                        .WithMany("Histories")
                        .HasForeignKey("PartnerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Final.Data.Entities.PlanTask", b =>
                {
                    b.HasOne("Final.Data.Entities.Event", "Event")
                        .WithMany("PlanTask")
                        .HasForeignKey("EventId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Final.Data.Entities.Partner")
                        .WithMany("PlanTask")
                        .HasForeignKey("PartnerId");

                    b.HasOne("Final.Data.Entities.User")
                        .WithMany("PlanTask")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Final.Data.Entities.PlanTaskPartner", b =>
                {
                    b.HasOne("Final.Data.Entities.PlanTask")
                        .WithMany("PlanTaskPartner")
                        .HasForeignKey("PlanTaskId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Final.Data.Entities.TaskVolunteer", b =>
                {
                    b.HasOne("Final.Data.Entities.User")
                        .WithMany("TaskVolunteer")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Final.Data.Entities.Taska", b =>
                {
                    b.HasOne("Final.Data.Entities.Event")
                        .WithMany("Tasks")
                        .HasForeignKey("EventId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Final.Data.Entities.Partner")
                        .WithMany("Tasks")
                        .HasForeignKey("PartnerId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Final.Data.Entities.User")
                        .WithMany("ResponsibleTask")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<System.Guid>", b =>
                {
                    b.HasOne("Final.Data.Entities.Role")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<System.Guid>", b =>
                {
                    b.HasOne("Final.Data.Entities.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<System.Guid>", b =>
                {
                    b.HasOne("Final.Data.Entities.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<System.Guid>", b =>
                {
                    b.HasOne("Final.Data.Entities.Role")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Final.Data.Entities.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<System.Guid>", b =>
                {
                    b.HasOne("Final.Data.Entities.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
