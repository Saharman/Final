﻿using Final.Core.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Configurations
{
        public static class ConectionConfiguration
        {// вынесли подключение БД из стартап в отдельный класс(для удобства)
            public static IServiceCollection AddConnectionProvider(this IServiceCollection services,
                IConfiguration conf)
            {
                services.AddDbContext<FinalContext>(opt => opt.UseSqlite(conf.GetConnectionString("Final"),
                    b => b.MigrationsAssembly("Final")));
                // подключаем mysql, достаем строку подключения с помощью iconfiguration
                return services;
            }

        public static IServiceCollection AddMSSQL(this IServiceCollection services,
                IConfiguration conf)
        {
            services.AddDbContext<FinalContext>(opt => opt.UseSqlServer(conf.GetConnectionString("FinalM"),
                b => b.MigrationsAssembly("Final")));
            // подключаем mysql, достаем строку подключения с помощью iconfiguration
            return services;
        }
    }
}
