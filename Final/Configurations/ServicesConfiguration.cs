﻿using Final.Core.EF;
using Final.Core.Repositories;
using Final.Core.Services;
using Final.Data.Entities;
using Final.Data.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NETCore.MailKit.Infrastructure.Internal;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using NETCore.MailKit.Extensions;
using System.Threading.Tasks;

namespace Final.Configurations
{
    public static class ServicesConfiguration
    {
        public static IServiceCollection AddRepositories( //вынесли подключение репозиториев (для удобства)
            this IServiceCollection services)
        {
            services
                .AddTransient<ICategoryRepository, CategoryRepository>()
                .AddTransient<IContactRepository, ContactRepository>()
                .AddTransient<IEventRepository, EventRepository>()
                .AddTransient<IHashTagRepository, HashTagRepository>()
                .AddTransient<IPartnerEventRepository, PartnerEventRepository>()
                .AddTransient<IPartnerHistoryRepository, PartnerHistoryRepository>()
                .AddTransient<IPartnerRepository, PartnerRepository>()
                .AddTransient<ISpeakerRepository, SpeakerRepository>()
                .AddTransient<ITaskRepository, TaskRepository>()
                .AddTransient<ITaskVolunteerRepository, TaskVolunteerRepository>()
                .AddTransient<IUserRepository, UserRepository>();

            return services;
        }

        public static IServiceCollection AddMailKit(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMailKit(optionBuilder =>
            {
                optionBuilder.UseMailKit(new MailKitOptions()
                {
                    Server = configuration["Smtp"],
                    Port = Convert.ToInt32(configuration["Port"]),
                    SenderName = configuration["Email"],
                    SenderEmail = configuration["Email"],

                    Account = configuration["Email"],
                    Password = configuration["Password"],
                    Security = true

                });
            });

            return services;
        }


        public static IServiceCollection AddServices( //вынесли подключение репозиториев (для удобства)
            this IServiceCollection services)
        {
            services
                .AddTransient<IFileLoader, FileLoader>();


            return services;
        }

        public static IServiceCollection AddCorsConfiguration(this IServiceCollection services) =>
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll", new Microsoft.AspNetCore.Cors.Infrastructure.CorsPolicyBuilder()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowAnyOrigin()
                    .AllowCredentials()
                    .Build());
            });

        public static IServiceCollection ConfigureIdentity(this IServiceCollection services)
        {
            services.AddIdentity<User, Role>(o =>
            {
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequireUppercase = false;
                o.Password.RequireDigit = false;
                o.Password.RequireLowercase = false;
            })
                .AddEntityFrameworkStores<FinalContext>();

            return services;
        }

        public static IServiceCollection AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });

            return services;
        }



    }
}
